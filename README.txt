INTRODUCTION
------------
The Credly module allows integration between your Drupal site and the Credly 
Open Credit API — a platform for creating, giving and earning credit digital 
badges.

This module currently:
 * Provides a tab on user accounts for awarding your organization's badges.
 * Displays earned badges on user profiles.


REQUIREMENTS
------------
You will need to:
 * Sign up your site/organization/self for Credly at credly.com.
 * Create badges with Credly's Badge Builder (available once you have a Credly 
 account).
 * Obtain API credentials for your site/organization from Credly. Learn more 
 about their API and how to get started here.
 * The PHP cURL library should be enabled - find more information here:
 http://php.net/manual/en/curl.setup.php


CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions
 * Enter Credly and Open Credit API credentials at Configuration » Content 
 authoring » Credly
 * Award badges at user/[uid]/credly 


AUTHOR
------
This module is developed by Major Robot Interactive (http://majorrobot.com), 
based on code written for the National Museum of Natural History's Q?rius site 
and exhibit (http://qrius.si.edu), in collaboration with C&G Partners 
(http://cgpartnersllc.com) and the folks at Credly (http://credly.com).
